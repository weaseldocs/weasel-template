package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Sample(c *gin.Context) {
	var request string
	err := json.NewDecoder(c.Request.Body).Decode(&request)
	if err != nil {
		c.JSON(http.StatusBadRequest, "")
		return
	}
	c.JSON(http.StatusOK, request)
}
